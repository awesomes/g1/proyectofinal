package com.company;

import static java.lang.Math.*;

public class Geometric {

    //Calculate the area for a square using one side
    public static int squareArea(int side)
    {   if(side < 0)
        throw new IllegalArgumentException();

        return (int) Math.pow(side, 2);

    }

    //Calculate the area for a square using two sides
    public static int squareArea(int sidea, int sideb)
    {
        if(sidea < 0 || sideb < 0)
            throw new IllegalArgumentException();

        return sidea * sideb;

    }

    //Calculate the area of circle with the radius
    public static double circleArea(double radius)

    {
        if(radius < 0) {
            throw new IllegalArgumentException();
        }

        return PI * Math.pow(radius, 2);

    }

    //Calculate the perimeter of circle with the diameter
    public static double circlePerimeter(int diameter)
    {

        return PI * diameter;
    }

    //Calculate the perimeter of square with a side
    public static double squarePerimeter(double side)
    {
        return side * side;
    }

    //Calculate the volume of the sphere with the radius
    public static double sphereVolume(double radius)
    {

        return (1.333333) * Math.PI * (radius * radius * radius);
    }

    //Calculate the area of regular pentagon with one side
    public static float pentagonArea(int side)
    {
        return (float) ((5 * Math.pow(side, 2)) / (4 * Math.tan((PI / 5))));
    }

    //Calculate the Hypotenuse with two cathetus
    public static double calculateHypotenuse(double catA, double catB)
    {
        double h = Math.sqrt(Math.pow(catA, 2) + Math.pow(catB, 2));
        return h;

    }

}



