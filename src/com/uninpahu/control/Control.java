package com.uninpahu.control;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Control {

    private static int volumen;
    private static int aux;
    private static Control control;

    public static Control getInstance(String accion) {
        if(control == null && accion.equalsIgnoreCase("On")){
            control = new Control();
        }

        return control;
    }

    public Long cambioCanal(String canal) {
        if(validarCanales(canal)) {
            return Long.parseLong(canal);
        } else if(canal.equalsIgnoreCase("+Ch") || canal.equalsIgnoreCase("+ Ch")) {
            return Long.parseLong(canal) + 1;
        } else if (canal.equalsIgnoreCase("+Ch") || canal.equalsIgnoreCase("+ Ch")) {
            return Long.parseLong(canal) - 1;
        }
        return Long.parseLong(canal);
    }

    public int subirBajarVolumen(String vol) {
        if (vol.equalsIgnoreCase("+ Vol") ||vol.equalsIgnoreCase("+Vol")) {
            ++volumen;
            if (volumen != 100) {
                return volumen;
            }
        } else if (vol.equalsIgnoreCase("- Vol") || vol.equalsIgnoreCase("-Vol")) {
            if (volumen > 0) {
                return --volumen;
            }
        } else if (vol.equalsIgnoreCase("Mute")) {
            aux = volumen;
            volumen = 0;
        } else if (vol.equalsIgnoreCase("Unmute")){
            volumen = aux;
        }

        return volumen;
    }

    public boolean validarCanales(String canalIngresado) {
        Pattern pattern = Pattern.compile("\\d*");
        Matcher encaja = pattern.matcher(canalIngresado);

        if((canalIngresado.length() > 0 && canalIngresado.length() <= 3) && encaja.matches()){
            return true;
        } else {
            return false;
        }

    }

}
