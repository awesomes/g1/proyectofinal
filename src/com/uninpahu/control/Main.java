package com.uninpahu.control;

import com.uninpahu.control.Control;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {

        Scanner sc = new Scanner(System.in);
        String accion;
        String opco;
        Long canal;

        do {
            System.out.println("*******************");
            System.out.println("***** On/Off ******");
            System.out.println("*******************");

            opco = sc.nextLine();

            if (opco.equalsIgnoreCase("On")) {
                Control control = Control.getInstance("On");

                do {
                    System.out.println("*******************");
                    System.out.println("***** On/Off ******");
                    System.out.println("** 1 *** 3 *** 3 **");
                    System.out.println("** 4 *** 5 *** 6 **");
                    System.out.println("** 7 *** 8 *** 9 **");
                    System.out.println("** + Vol ** + Ch **");
                    System.out.println("** - Vol ** - Ch **");
                    System.out.println("****** Mute *******");
                    System.out.println("***** UnMute ******");
                    System.out.println("*******************");

                    Scanner acciones = new Scanner(System.in);
                    accion = sc.nextLine();
                    canal = 0L;

                    if ((accion.equalsIgnoreCase("+ Vol") || accion.equalsIgnoreCase("- Vol")) || (accion.equalsIgnoreCase("+Vol") || accion.equalsIgnoreCase("-Vol")) || accion.equalsIgnoreCase("Mute") || accion.equalsIgnoreCase("Unmute")) {
                        int vol = control.subirBajarVolumen(accion);
                        System.out.println("El volumen se encuentra en " + vol);
                    } else if(control.validarCanales(accion)) {
                        canal = control.cambioCanal(accion);
                        System.out.println("El canal se cambio a " + canal);
                    } else if ((accion.equalsIgnoreCase("+ Ch") || accion.equalsIgnoreCase("- Ch")) || (accion.equalsIgnoreCase("+Ch") || accion.equalsIgnoreCase("-Ch"))) {

                    } else if (accion.equalsIgnoreCase("On")) {
                        throw new Exception("El TV ya se encuentra encendido");
                    }
                } while (!accion.equalsIgnoreCase("Off"));

            }
        } while(opco.equalsIgnoreCase("On"));
    }


}
