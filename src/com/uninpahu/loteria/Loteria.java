package com.uninpahu.loteria;

public class Loteria {

    private static Loteria loteria;

    private Loteria() {
    }

    public static Loteria getInstance() {
        if(loteria == null)
            loteria = new Loteria();

        return loteria;
    }

    public int[] generarNumeroGanador() {
        int winnersNumber[] = new int[3];

        for(int i = 0; i < 3; i++) {
            winnersNumber[i] = (int) (Math.random() * 1000);
        }
        return winnersNumber;
    }

    public void validacionGanador(int[] playNumbers) {
        int[] numerosGenerados = generarNumeroGanador();

        for(int i = 0; i < numerosGenerados.length; i++) {
            for (int j = 0; j < playNumbers.length; j++) {
                if(numerosGenerados[i] == playNumbers[j]) {
                    System.out.println("HA GANADO!! El número generado por la lotería es " + numerosGenerados[i] + " y el número ingresado ganador es " + playNumbers[j]);
                    break;
                } else {
                    System.out.println("Se válido el número " + playNumbers[j] + " vs " + numerosGenerados[i] + " \nLastimosamente NO HA GANADO");
                }
            }
        }
    }
}