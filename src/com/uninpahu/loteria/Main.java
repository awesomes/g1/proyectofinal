package com.uninpahu.loteria;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        Loteria l = Loteria.getInstance();
        int[] numerosIngresados = new int[3];

        System.out.println("Ingrese un número de tres dígitos para jugar a la lotería");

        for(int i = 0; i < 3; i++) {
            numerosIngresados[i] = sc.nextInt();
        }

        l.validacionGanador(numerosIngresados);

    }

}
