package com.uninpahu.main;

import com.uninpahu.loteria.Loteria;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int option = 0;

        System.out.println("Ingrese una opción\n 1.Jugar Lotería\n2.Simulador Control TV\n0.Salir");
        option = sc.nextInt();

        do {
            switch (option) {
                case 1:

                    Loteria loteria = Loteria.getInstance();
                    int[] numerosIngresados = new int[3];

                    System.out.println("Ingrese un número de tres dígitos para jugar a la lotería");

                    for(int i = 0; i < 3; i++) {
                        numerosIngresados[i] = sc.nextInt();
                    }

                    loteria.validacionGanador(numerosIngresados);

            }
        }while (option != 0);

    }

}
